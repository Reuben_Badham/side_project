# Menu Information


**This document will have some useful information on how the program is expected to run**

## Program functions

### Menu

- This should be the first thing that the user sees when they start up the program. 
- This should hold printed text that gives the user some basic information about what each function does. 
- It should prompt the user to input the number for the function they would like to run. 
- Each number of the menu should lead to the function of the desired menu item. 

### Remove a letter

- This should start up when the user types 1 in the menu.
- Should tell the user which option they chose as confirmation.
- This function should hold a global variable so that it can be used in the three sections of the program. 
- The first thing that the function should do is check to see if there is already anything in "saved_string"
- If there is nothing in the string then the function should say so and then prompt the user to create a string.
- If there is something save to "saved_string" then the content of the string should be printed and the user prompted to edit the string/overwrite it.
- When the user has reworded/edited/overwitten the "saved_string" then the menu should be called and the menu be visable again. 

### Number compare

- This should start up when the user types 2 in the menu.
- Should tell the user which option they chose as confirmation.
- First it should prompt the user to input the first number
- Then it should prompt the user for the second input. 
- Finally the program should determine which number is larger and print the larger of the two inputs.
- The program should then end and take the user back to the menu. 

### Print string

- This should start up when the user types 3 in the menu.
- This should print the contents of "saved_string" 
- If nothing is stored in the variable then it should say so and take you back to the menu.

### Calculator

- This should start up when the user types 4 in the menu.
- It should prompt the user for number 1.
- It should then prompt the user to chose and operator from the suggested. 
- Then prompt the user for the second input.
- The program should then calculate the inputs and present the output.
- Return to the menu.

### Accept and store

- This should start up when the user types 5 in the menu.
- It should pull the gobal variable
- Then the user should be able to save a string into the variable. 
- menu should be called

### Key logger 

- This should start up when the user types 6 in the menu. 
- The listener should join, waiting for button presses.
- If a button is pressed the listener should register it and store the button press in log.text
- If a space is entered a new line should be created in log.text
- log.txt should be updated every 1 button press as to keep it updated frequently. 
- If esc is pressed then the key logger should stop return the user to the menu. 

### Flow chart

![](https://gitlab.com/Reuben_Badham/side_project/-/raw/master/block_diagram_menu.png?inline=false)

- This is a flow chart of how the program should run.


## Bugs

### Key logger
- When i try to end the key logger and call the menu. It takes you back to the menu but if you then try to exit the prgram it causes a bunch of errors.

