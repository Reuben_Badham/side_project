import pynput
import time
import sys
from pynput.keyboard import Key, Listener


count = 0
keys = []
saved_string = ""

def on_press(key):
    global keys, count

    keys.append(key)
    count += 1
    print("{0} pressed".format(key))

    if count >= 1:
        count = 0
        write_file(keys)
        keys = []

def write_file(keys):
    with open("log.txt", "a") as f:
        for key in keys:
            k = str(key).replace("'"," ")
            if k.find("space") > 0:
                f.write("\n")
            elif k.find("Key") == -1:
                f.write(k)

def on_release(key):
    if key == Key.esc:

        return False + menu()


def KeyLogger():
    print("'Escape button' will quit the keylogger.\n"
          "Typing will store the keystrokes into 'log.txt' which you will have to open to see the stored data.")
    with Listener(on_press=on_press, on_release=on_release) as listener:
        listener.join()

###########################################################################################

def remove_letter(): #remove a selected letter of a string
    print("You chose 'Edit or change the existing string'")
    global saved_string

    if len(saved_string) == 0:
        print("There was no previously store string")
        saved_string = str(input("Please type a new string to be stored: "))
    else:
        print("The previously saved string was: " + "'" + saved_string + "'")
        saved_string = str(input("Please edit or enter a new string: "))

    time.sleep(2)
    menu()
    return

###########################################################################################

def num_compare(): #compare two numbers to determine the larger
    print("num compare")

    try:
        num1 = int(input("Enter first number: "))
        num2 = int(input("Enter second number: "))
        if num1 > num2:
            print(num1)
        else:
            print(num2)
    except ValueError:
        print("That's not a number")
        choice = str(input("would you like to try again? y/n: "))
        if choice == "y":
            num_compare()
        else:
            time.sleep(2)
            menu()
    time.sleep(2)
    menu()
    return

###########################################################################################

def print_string(): #print the previously stored string
    if saved_string == "":
        print("No string has been stored yet\n"
              "Please try menu no.5 to store a string")
        time.sleep(4)
        menu()
    else:
        print("The previously stored string was")
        print(saved_string)
    time.sleep(2)
    menu()
    return

###########################################################################################

def calculator(): #Basic Calculator



    try:
        num1 = int(input("Input first number: "))
        sign = str(input("Choose operator (+,-,/,*): "))
        num2 = int(input("Input second number: "))
        if sign == "*":
            print(num1 * num2)
        elif sign == "/":
            print(num1 / num2)
        elif sign == "-":
            print(num1 - num2)
        elif sign == "+":
            print(num1 + num2)
        else:
            print("Invalid operator")

    except ValueError:
        print("That's not a number")
        choice = str(input("would you like to try again? y/n: "))
        if choice == "y":
            calculator()
        else:
            time.sleep(2)
            menu()
    time.sleep(2)
    menu()
    return

###########################################################################################

def accept_and_store(): #accept and store a string
    global saved_string
    saved_string = str(input("Input string: "))
    time.sleep(2)
    menu()
    return

###########################################################################################

def menu(): #menu goes here
    print("\nThe menu:\n"
          "1. Edit or change the existing string\n"
          "2. Compare two numbers to determine the larger\n"
          "3. Print the previously stored string\n"
          "4. Basic Calculator\n"
          "5. Accept and store a string\n"
          "6. Key Logger\n"
          "10. Exit the program\n")

    Menuitem = int(input("please enter a number for the task you would like to run: "))

    if Menuitem == 1:
        remove_letter()
    if Menuitem == 2:
        num_compare()
    if Menuitem == 3:
        print_string()
    if Menuitem == 4:
        calculator()
    if Menuitem == 5:
        accept_and_store()
    if Menuitem == 6:
        KeyLogger()
    if Menuitem == 10:
        print("Have a good day")
        sys.exit

    return


menu()